import { Given } from "cypress-cucumber-preprocessor/steps";

const url = 'http://qweb-universe-temp.qweb.qos/';

Given('I navigate to Universe page', () => {
  cy.visit(url);
})